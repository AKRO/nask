package core;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import exceptions.SiteUnreachableException;
import utils.Dictionary;

@Path("/characters")
public class CharactersWebService {
	final Logger logger = Logger.getLogger(this.getClass());
	SwApi swApi = new SwApi();
	String results;

	@GET
	@Path("/{id}")
	public String getCharacterDetails(@PathParam("id") int id) throws ClientProtocolException, IOException {
		try {
			results = swApi.getCharacterDetails(id);
		} catch (SiteUnreachableException e) {
			BasicConfigurator.configure();
			logger.error(e.getStackTrace());
			results = Dictionary.ERROR_OCCURED;
		}
		return results;
	}

	@GET
	public String getCharacters(@QueryParam("page") int page) {
		try {
			results = swApi.getCharacters(page);
		} catch (SiteUnreachableException e) {
			BasicConfigurator.configure();
			logger.error(e.getStackTrace());
			results = Dictionary.ERROR_OCCURED;
		}
		return results;
	}
}