package core;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

import controller.CallsToSwApi;
import controller.DtoCreator;
import controller.JsonOperations;
import exceptions.SiteUnreachableException;
import model.Character;
import model.Page;
import utils.Dictionary;

public class SwApi {

	final Logger logger = Logger.getLogger(this.getClass());
	JsonOperations jsonOperations = new JsonOperations();
	ObjectMapper objMapper = new ObjectMapper();
	DtoCreator dtoCreator = new DtoCreator();
	CallsToSwApi callsToSwApi = new CallsToSwApi();
	String jsonStr;
	JsonObject json;

	public String getCharacterDetails(int id) throws SiteUnreachableException {
		json = doSwApiCall(Dictionary.SWAPI_URL_PERSON + id + "/");
		if (json != null) {
			try {
				Character character = new Character();
				character = dtoCreator.fillCharacter(id, json);
				jsonStr = objMapper.writeValueAsString(character);
			} catch (JsonProcessingException e) {
				BasicConfigurator.configure();
				logger.error(e.getStackTrace());
				jsonStr = Dictionary.ERROR_OCCURED;
			}
		} else {
			jsonStr = Dictionary.RETURNED_ERROR;
			BasicConfigurator.configure();
			logger.error(Dictionary.SITE_UNREACHABLE);
			throw new SiteUnreachableException(Dictionary.SITE_UNREACHABLE);
		}

		return jsonStr;
	}

	public String getCharacters(int page) throws SiteUnreachableException {
		json = doSwApiCall(Dictionary.SWAPI_URL_PEOPLE + page);
		if (json != null) {
			try {
				Page _page = new Page();
				_page = dtoCreator.fillPage(json);
				jsonStr = objMapper.writeValueAsString(_page);
			} catch (JsonProcessingException e) {
				BasicConfigurator.configure();
				logger.error(e.getStackTrace());
				jsonStr = Dictionary.ERROR_OCCURED;
			}
		} else {
			jsonStr = Dictionary.RETURNED_ERROR;
			BasicConfigurator.configure();
			logger.error(Dictionary.SITE_UNREACHABLE);
			throw new SiteUnreachableException(Dictionary.SITE_UNREACHABLE);
		}

		return jsonStr;
	}

	private JsonObject doSwApiCall(String urlToUse) throws SiteUnreachableException {
		String resultFromGetRequest = callsToSwApi.makeCallToSwApi(urlToUse);
		json = jsonOperations.stringToJson(resultFromGetRequest);

		return json;
	}
}