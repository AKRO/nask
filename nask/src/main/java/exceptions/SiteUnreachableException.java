package exceptions;

public class SiteUnreachableException extends Exception {
	private static final long serialVersionUID = 200885367385576608L;

	public SiteUnreachableException(String errorMessage) {
		super(errorMessage);
	}

}
