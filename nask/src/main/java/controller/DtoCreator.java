package controller;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import exceptions.SiteUnreachableException;
import model.Character;
import model.Homeworld;
import model.Page;
import model.Starship;
import utils.UrlEditor;

public class DtoCreator {
	CallsToSwApi callsToSwApi = new CallsToSwApi();
	JsonOperations jsonOperations = new JsonOperations();

	public Character fillCharacter(int id, JsonObject json) throws SiteUnreachableException {
		Character character = new Character();

		character.setId(id);
		character.setName(json.get("name").getAsString());
		character.setHeight(json.get("height").getAsString());
		character.setMass(json.get("mass").getAsString());
		character.setHairColor(json.get("hair_color").getAsString());
		character.setSkinColor(json.get("skin_color").getAsString());
		character.setEyeColor(json.get("eye_color").getAsString());
		character.setBirthYear(json.get("birth_year").getAsString());
		character.setGender(json.get("gender").getAsString());

		String homeworldData = callsToSwApi.makeCallToSwApi(json.get("homeworld").getAsString());
		JsonObject homeworldJson = jsonOperations.stringToJson(homeworldData);
		character.setHomeworld(fillHomeworld(homeworldJson));

		List<Starship> starships = new ArrayList<>();
		JsonArray starshipsArray = json.get("starships").getAsJsonArray();
		for (JsonElement jsonElem : starshipsArray) {
			String starshipData = callsToSwApi.makeCallToSwApi(jsonElem.getAsString());
			JsonObject starshipJson = jsonOperations.stringToJson(starshipData);
			starships.add(fillStarship(starshipJson));
		}
		character.setStarships(starships);

		return character;
	}

	public Homeworld fillHomeworld(JsonObject json) {
		Homeworld homeworld = new Homeworld();

		homeworld.setName(json.get("name").getAsString());
		homeworld.setRotationPeriod(json.get("rotation_period").getAsString());
		homeworld.setOrbitalPeriod(json.get("orbital_period").getAsString());
		homeworld.setDiameter(json.get("diameter").getAsString());
		homeworld.setClimate(json.get("climate").getAsString());
		homeworld.setGravity(json.get("gravity").getAsString());
		homeworld.setTerrain(json.get("terrain").getAsString());
		homeworld.setSurfaceWater(json.get("surface_water").getAsString());
		homeworld.setPopulation(json.get("population").getAsString());

		return homeworld;
	}

	public Starship fillStarship(JsonObject json) {
		Starship starship = new Starship();

		starship.setName(json.get("name").getAsString());
		starship.setModel(json.get("model").getAsString());
		starship.setManufacturer(json.get("manufacturer").getAsString());
		starship.setCostInCredits(json.get("cost_in_credits").getAsString());
		starship.setLength(json.get("length").getAsString());
		starship.setMaxAtmosphericSpeed(json.get("max_atmosphering_speed").getAsString());
		starship.setCrew(json.get("crew").getAsString());
		starship.setPassengers(json.get("passengers").getAsString());
		starship.setCargoCapacity(json.get("cargo_capacity").getAsString());
		starship.setConsumables(json.get("consumables").getAsString());
		starship.setHyperdriveRating(json.get("hyperdrive_rating").getAsString());
		starship.setMglt(json.get("MGLT").getAsString());
		starship.setStarshipClass(json.get("starship_class").getAsString());

		return starship;
	}

	public Page fillPage(JsonObject json) throws SiteUnreachableException {
		Page page = new Page();
		UrlEditor urlEditor = new UrlEditor();

		page.setCount(json.get("count").getAsInt());
		page.setPages(json.get("count").getAsInt() / 10 + 1);

		JsonArray resultsArray = json.get("results").getAsJsonArray();
		List<Character> characters = new ArrayList<>();
		for (JsonElement jsonElem : resultsArray) {
			JsonObject jsonObj = jsonElem.getAsJsonObject();
			String url = jsonObj.get("url").getAsString();
			String characterData = callsToSwApi.makeCallToSwApi(url);
			JsonObject characterJson = jsonOperations.stringToJson(characterData);
			characters.add(fillCharacter(Integer.parseInt(urlEditor.getIdFromUrl(url)), characterJson));
		}
		page.setElements(characters);

		return page;
	}
}
