package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import exceptions.SiteUnreachableException;
import utils.Dictionary;

public class CallsToSwApi {

	public String makeCallToSwApi(String url) throws SiteUnreachableException {
		StringBuilder stringBuilder = new StringBuilder();
		final Logger logger = Logger.getLogger(this.getClass());

		try {
			HttpGet httpGet = new HttpGet(url);
			HttpClient httpClient = HttpClientBuilder.create().build();
			httpGet.addHeader("accept", "application/json");
			HttpResponse response;
			response = httpClient.execute(httpGet);

			if (response.getStatusLine().getStatusCode() == 404) {
				BasicConfigurator.configure();
				logger.error(Dictionary.SITE_UNREACHABLE);
				throw new SiteUnreachableException(Dictionary.SITE_UNREACHABLE);
			}

			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader((response.getEntity().getContent())));
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line);
			}
			bufferedReader.close();
		} catch (IOException e) {
			BasicConfigurator.configure();
			logger.error(e.getStackTrace());
		}
		return stringBuilder.toString();
	}
}
