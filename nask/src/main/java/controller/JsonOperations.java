package controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

public class JsonOperations {

	public JsonObject stringToJson(String str) {
		JsonObject json = null;
		Gson gson = new Gson();
		try {
			json = gson.fromJson(str, JsonObject.class);
		} catch (JsonSyntaxException exc) {
			exc.printStackTrace();
		}
		return json;
	}

}
