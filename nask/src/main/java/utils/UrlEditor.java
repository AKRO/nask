package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlEditor {
	public String getIdFromUrl(String url) {
		Pattern pattern = Pattern.compile("\\d+");
		Matcher id = pattern.matcher(url);
		id.find();

		return id.group();
	}
}
