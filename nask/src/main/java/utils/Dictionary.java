package utils;

public class Dictionary {
	public static final String SITE_UNREACHABLE = "Error code 500: site returned code 404 so it may be down or not existing.";
	public static final String ERROR_OCCURED = "Error occured in WebService class. Please check logs for more info.";

	// SwApi class
	public static final String SWAPI_BASE_URL = "https://swapi.dev/api/";
	public static final String SWAPI_URL_PERSON = SWAPI_BASE_URL + "people/";
	public static final String SWAPI_URL_PEOPLE = SWAPI_BASE_URL + "people/?page=";
	public static final String RETURNED_ERROR = "Cannot reach requested site.";
}
