package model;

import java.util.List;

public class Page {
	private int count;
	private int pages;
	private List<Character> elements;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int page) {
		this.pages = page;
	}

	public List<Character> getElements() {
		return elements;
	}

	public void setElements(List<Character> elements) {
		this.elements = elements;
	}

}
