package core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import exceptions.SiteUnreachableException;
import utils.TestDictionary;

public class SwApiTest {
	@Test
	public void getCharacterDetails() throws SiteUnreachableException {
		SwApi swApi = new SwApi();
		assertEquals(TestDictionary.CHARACTER_TEST, swApi.getCharacterDetails(1));
	}

	@Test
	public void getCharacters() throws SiteUnreachableException {
		SwApi swApi = new SwApi();
		assertEquals(TestDictionary.PAGE_TEST, swApi.getCharacters(1));
	}

}
