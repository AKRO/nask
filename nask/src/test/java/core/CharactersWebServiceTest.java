package core;

import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.junit.jupiter.api.Test;

public class CharactersWebServiceTest {
	@Test
	public void getCharacterDetailsTest() throws ClientProtocolException, IOException {
		CharactersWebService charactersWebService = new CharactersWebService();
		String results = charactersWebService.getCharacterDetails(100);
		assertNull(results);
	}

	@Test
	public void getCharactersTest() {
		CharactersWebService charactersWebService = new CharactersWebService();
		String results = charactersWebService.getCharacters(100);
		assertNull(results);
	}

}
