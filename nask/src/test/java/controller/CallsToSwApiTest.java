package controller;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import exceptions.SiteUnreachableException;
import utils.TestDictionary;

public class CallsToSwApiTest {
	CallsToSwApi callsToSwApi = new CallsToSwApi();

	@Test
	public void makeCallToSwApiTestPass() throws SiteUnreachableException {
		assertDoesNotThrow(() -> {
			callsToSwApi.makeCallToSwApi(TestDictionary.GOOD_URL);
		});

	}

	@Test
	public void makeCallToSwApiTestFail() throws SiteUnreachableException {

		assertThrows(SiteUnreachableException.class, () -> {
			callsToSwApi.makeCallToSwApi(TestDictionary.BAD_URL);
		});
	}
}
