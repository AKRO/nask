package controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

import utils.TestDictionary;

public class JsonOperationsTest {
	@Test
	public void stringToJsonTest() {
		JsonOperations jsonOperations = new JsonOperations();
		assertEquals(JsonObject.class, jsonOperations.stringToJson(TestDictionary.CHARACTER_TEST).getClass());
	}

}
