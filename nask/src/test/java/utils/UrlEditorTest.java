package utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class UrlEditorTest {
	@Test
	public void getCorrectId() {
		UrlEditor urlEditor = new UrlEditor();
		String result = urlEditor.getIdFromUrl(Dictionary.SWAPI_URL_PERSON + "123");
		assertEquals("123", result);
	}
}
