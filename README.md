Recruitment task<br>
Built with maven and deployed on TomEE. On two available endpoints retrieves data from swapi.dev regarding details about characters (http://localhost:8080/nask/characters/1) or list of available characters (http://localhost:8080/nask/characters?page=2). 


***PERSONAL NOTES***
<br>**Incorrect way:** <br>
    Client client = ClientBuilder.newClient();<br>
	WebTarget webTarget = client.target(swApiUrlDetails + id);<br>
	Builder request = webTarget.request(MediaType.APPLICATION_JSON);<br>
	Invocation method = request.buildGet();<br>
	Response response = method.invoke();<br>
	System.out.println(id); <br>
(not working - either it shows the swapi site, not even reaching the printing of id or nothing with code 301 and printing the id) <br>
**Correct way:**<br>
    HttpGet httpGet = new HttpGet(url);<br>
	HttpClient httpClient = HttpClientBuilder.create().build();<br>
	httpGet.addHeader("accept", "application/json");<br>
	HttpResponse response;<br>
	response = httpClient.execute(httpGet); <br>
(works but for some values it opens the swapi site (e.g. 1, 10) - this happened due to a slash missing at the end of the url)